#ifndef MY_API_OPS
#define MY_API_OPS

#include <mpi.h>

OMPI_DECLSPEC int my_MPI_Bcast(void *buffer, int count, MPI_Datatype datatype, int root, MPI_Comm comm)
{
    int rank;
    MPI_Comm_rank(comm, &rank);
    int numprocs;
    MPI_Comm_size(comm, &numprocs);

    if (rank == root)
    {
        // If we are the root process, send our data to everyone
        int i;
        for (i = 0; i < numprocs; i++)
        {
            if (i != rank)
            {
                MPI_Send(buffer, count, datatype, i, 0, comm);
            }
        }
    }
    else
    {
        // If we are a receiver process, receive the data from the root
        MPI_Recv(buffer, count, datatype, root, 0, comm,
                 MPI_STATUS_IGNORE);
    }
    return 0;
}

OMPI_DECLSPEC int my_MPI_Bcast_fast(void *buffer, int count, MPI_Datatype datatype, int root, MPI_Comm comm)
{
    int rank;
    MPI_Comm_rank(comm, &rank);
    int numprocs;
    MPI_Comm_size(comm, &numprocs);

    if (rank == root)
    {
        for(int next_to_send = numprocs / 2; next_to_send >= rank; next_to_send /= 2)
            MPI_Send(buffer, count, datatype, next_to_send, 0, comm);
    }
    else
    {
        // If we are a receiver process, receive the data from the root
        MPI_Recv(buffer, count, datatype, MPI_ANY_SOURCE, 0, comm,
                 MPI_STATUS_IGNORE);
        int offset = -1;
        for(int i = numprocs / 2; i>=2;i/=2){
            if(rank%i == 0){
                offset = i/2;
                break;
            }
        }
        if(offset != -1){
            for(; offset >= 1; offset /= 2)
                MPI_Send(buffer, count, datatype, rank+offset, 0, comm);
        }
    }
    return 0;
}

#endif