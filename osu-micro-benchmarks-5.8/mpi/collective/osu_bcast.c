#define BENCHMARK "OSU MPI%s Broadcast Latency Test"
/*
 * Copyright (C) 2002-2021 the Network-Based Computing Laboratory
 * (NBCL), The Ohio State University.
 *
 * Contact: Dr. D. K. Panda (panda@cse.ohio-state.edu)
 *
 * For detailed copyright and licensing information, please refer to the
 * copyright file COPYRIGHT in the top level OMB directory.
 */
#include <osu_util_mpi.h>
#include "/home/nadavhalahmi/mpi-playground/my_api_ops.h"

long validate_bcast(char *buffer, char* expected_buffer, size_t size){
    long errors = 0;
    for(size_t i=0;i<size;i++){
        if(buffer[i] != expected_buffer[i])
            errors++;
    }
    return errors;
}

int main(int argc, char *argv[])
{
    int i = 0, rank, size;
    long errors = 0;
    long total_errors = 0;
    int numprocs;
    double avg_time = 0.0, max_time = 0.0, min_time = 0.0;
    double latency = 0.0, t_start = 0.0, t_stop = 0.0;
    double timer=0.0;
    char *buffer=NULL;
    char *expected_buffer = NULL;
    int po_ret;
    options.bench = COLLECTIVE;
    options.subtype = LAT;
    MPI_Request request;

    set_header(HEADER);
    set_benchmark_name("osu_bcast");
    po_ret = process_options(argc, argv);

    if (PO_OKAY == po_ret && NONE != options.accel) {
        if (init_accel()) {
            fprintf(stderr, "Error initializing device\n");
            exit(EXIT_FAILURE);
        }
    }

    MPI_CHECK(MPI_Init(&argc, &argv));
    MPI_CHECK(MPI_Comm_rank(MPI_COMM_WORLD, &rank));
    MPI_CHECK(MPI_Comm_size(MPI_COMM_WORLD, &numprocs));

    switch (po_ret) {
        case PO_BAD_USAGE:
            print_bad_usage_message(rank);
            MPI_CHECK(MPI_Finalize());
            exit(EXIT_FAILURE);
        case PO_HELP_MESSAGE:
            print_help_message(rank);
            MPI_CHECK(MPI_Finalize());
            exit(EXIT_SUCCESS);
        case PO_VERSION_MESSAGE:
            print_version_message(rank);
            MPI_CHECK(MPI_Finalize());
            exit(EXIT_SUCCESS);
        case PO_OKAY:
            break;
    }

    if (numprocs < 2) {
        if (rank == 0) {
            fprintf(stderr, "This test requires at least two processes\n");
        }

        MPI_CHECK(MPI_Finalize());
        exit(EXIT_FAILURE);
    }

    if (options.max_message_size > options.max_mem_limit) {
        if (rank == 0) {
            fprintf(stderr, "Warning! Increase the Max Memory Limit to be able to run up to %ld bytes.\n"
                            "Continuing with max message size of %ld bytes\n", 
                            options.max_message_size, options.max_mem_limit);
        }
        options.max_message_size = options.max_mem_limit;
    }

    if(options.validate){ // initialize expected_buffer to 1's in all proccesses
        if (allocate_memory_coll((void**)&expected_buffer, options.max_message_size, options.accel)) {
            fprintf(stderr, "Could Not Allocate Memory [rank %d]\n", rank);
            MPI_CHECK(MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE));
        }
        set_buffer(expected_buffer, options.accel, 1, options.max_message_size);
    }

    if (allocate_memory_coll((void**)&buffer, options.max_message_size, options.accel)) {
        fprintf(stderr, "Could Not Allocate Memory [rank %d]\n", rank);
        MPI_CHECK(MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE));
    }
    if(rank == 0) // initilize only rank0 buffer to 1 to make sure others get it
        set_buffer(buffer, options.accel, 1, options.max_message_size);
    else
        set_buffer(buffer, options.accel, 0, options.max_message_size);

    if(rank == 0) printf("\nrunning bcast with algorithm: %s\n", options.algorithm);
    
    print_preamble(rank);

    for (size=options.min_message_size; size <= options.max_message_size; size *= 2) {
        if (size > LARGE_MESSAGE_SIZE) {
            options.skip = options.skip_large; 
            options.iterations = options.iterations_large;
        }

        timer=0.0;
        errors = total_errors = 0;  
        for (i=0; i < options.iterations + options.skip ; i++) {
            if(rank != 0){ // reset buffer per iteration
                set_buffer(buffer, options.accel, 0, size);   
            }    
            t_start = MPI_Wtime();
            if(!strcmp(options.algorithm, "my"))
                MPI_CHECK(my_MPI_Bcast(buffer, size, MPI_CHAR, 0, MPI_COMM_WORLD));
            else if(!strcmp(options.algorithm, "my_fast"))
                MPI_CHECK(my_MPI_Bcast_fast(buffer, size, MPI_CHAR, 0, MPI_COMM_WORLD));
            else if(!strcmp(options.algorithm, "ibcast")){
                MPI_CHECK(MPI_Ibcast(buffer, size, MPI_CHAR, 0, MPI_COMM_WORLD, &request));
                MPI_Wait(&request, NULL);
            }
            else
                MPI_CHECK(MPI_Bcast(buffer, size, MPI_CHAR, 0, MPI_COMM_WORLD));
            t_stop = MPI_Wtime();

            if (i>=options.skip){
                timer+=t_stop-t_start;
            }
            // ~~~~~~~~ my code ~~~~~~~~
            if (options.validate) {
                errors += validate_bcast(buffer, expected_buffer, size);
            }
            // // ~~~~~ end my code ~~~~
            MPI_CHECK(MPI_Barrier(MPI_COMM_WORLD));

        }

        MPI_CHECK(MPI_Barrier(MPI_COMM_WORLD));

        latency = (timer * 1e6) / options.iterations;

        MPI_CHECK(MPI_Reduce(&latency, &min_time, 1, MPI_DOUBLE, MPI_MIN, 0,
                MPI_COMM_WORLD));
        MPI_CHECK(MPI_Reduce(&latency, &max_time, 1, MPI_DOUBLE, MPI_MAX, 0,
                MPI_COMM_WORLD));
        MPI_CHECK(MPI_Reduce(&latency, &avg_time, 1, MPI_DOUBLE, MPI_SUM, 0,
                MPI_COMM_WORLD));
        MPI_CHECK(MPI_Reduce(&errors, &total_errors, 1, MPI_LONG, MPI_SUM, 0, MPI_COMM_WORLD)); // sum all errors from all proccesses
        avg_time = avg_time/numprocs;
        if(options.validate){
            print_stats_validate(rank, size, avg_time, min_time, max_time, total_errors);}
        else
            print_stats(rank, size, avg_time, min_time, max_time);
    }

    free_buffer(buffer, options.accel);
    if(options.validate) free_buffer(expected_buffer, options.accel);

    MPI_CHECK(MPI_Finalize());

    if (NONE != options.accel) {
        if (cleanup_accel()) {
            fprintf(stderr, "Error cleaning up device\n");
            exit(EXIT_FAILURE);
        }
    }

    return EXIT_SUCCESS;
}

/* vi: set sw=4 sts=4 tw=80: */
